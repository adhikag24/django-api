CREATE TABLE IF NOT EXISTS djangoapp_user(
		id SERIAL PRIMARY KEY,
		Userid VARCHAR(50),
		Name   VARCHAR(50)
	);
	
with data(Userid, Name)  as (
	values
	   ('01', 'Budi'),
	   ('02', 'Nano')
     ) 
insert into djangoapp_user (Userid, Name) 
select d.Userid, d.Name
from data d
where not exists (select * from djangoapp_user);